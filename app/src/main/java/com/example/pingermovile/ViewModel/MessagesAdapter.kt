package com.example.pingermovile.ViewModel

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pingermovile.Model.Messages
import com.example.pingermovile.R
import com.example.pingermovile.databinding.ItemMessagesBinding
import com.squareup.picasso.Picasso


class MessagesAdapter (val messages: List<Messages>):RecyclerView.Adapter<MessagesAdapter.MessageHolder>(){

    private lateinit var  binding: ItemMessagesBinding


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageHolder {
        val layoutInflater= LayoutInflater.from(parent.context)
        return MessageHolder(layoutInflater.inflate(R.layout.item_messages, parent,false))
    }

    override fun onBindViewHolder(holder: MessageHolder, position: Int) {
        holder.render(messages[position])
    }

    override fun getItemCount(): Int = messages.size

    class MessageHolder(view:View):RecyclerView.ViewHolder(view){
        val binding = ItemMessagesBinding.bind(view)

        fun render(messages: Messages){

            binding.tvMensaje.text = messages.body
            Picasso.get().load(messages.img).into(binding.imgMessage)
        }


    }


}