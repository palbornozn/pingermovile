package com.example.pingermovile.ViewModel

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pingermovile.Model.Machines
import com.example.pingermovile.R
import com.example.pingermovile.databinding.ItemMachineBinding
import com.squareup.picasso.Picasso


class MachineAdapter(val machines: List<Machines>):RecyclerView.Adapter<MachineAdapter.MachineHolder>(){

    private lateinit var mListener: onItemClickListener

    interface onItemClickListener{

        fun onIntemClick(position : Int)
    }


    fun setOnItemCLickListener(listener: onItemClickListener){

        mListener = listener

    }




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MachineHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        return MachineHolder(layoutInflater.inflate(R.layout.item_machine, parent,false),mListener)


    }

    override fun onBindViewHolder(holder: MachineHolder, position: Int) {
        holder.render(machines[position])


    }

    override fun getItemCount(): Int  = machines.size



    class MachineHolder(view:View, listener: onItemClickListener):RecyclerView.ViewHolder(view){
       val binding = ItemMachineBinding.bind(view)

        init {
            itemView.setOnClickListener{

                listener.onIntemClick(adapterPosition)

            }

        }

        fun render(machines: Machines){

            binding.machineId.text = machines.name
            binding.lecturaId.text = machines.lectura
            binding.sensorId.text = machines.sensor
            Picasso.get().load(machines.imagen).into(binding.imgId)


        }

    }



}