package com.example.pingermovile

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.pingermovile.Model.Personaje
import com.example.pingermovile.Model.Usuario
import com.example.pingermovile.api.API
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import com.google.gson.GsonBuilder

import com.google.gson.Gson
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import org.json.JSONObject





class MainActivity : AppCompatActivity() {

    val service = createService()


    lateinit var respuesta: JsonObject ;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)




        var btnSaludar = findViewById<Button>(R.id.btnLogin);


        btnSaludar.setOnClickListener {

            val usuario = findViewById<EditText>(R.id.txtUsuario);
            val contrasena = findViewById<EditText>(R.id.txtContraseña);

            if (usuario.text.toString() == "") {

                Toast.makeText(this@MainActivity,"Ingrese usuario",Toast.LENGTH_SHORT).show()

            } else {

                executeLogin(usuario.text.toString(),contrasena.text.toString())

            }

        }

    }

    private fun executeLogin(usuario: String, contrasena: String) {

        val call = service.login(usuario,contrasena)

        call.enqueue(object :Callback<JsonObject>{

            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                if(response.isSuccessful)
                {

                    var miShared= getSharedPreferences("Memoria",Context.MODE_PRIVATE)

                    respuesta = response.body()!!

                    val objeto = response.body()!!.getAsJsonObject("Obj")

                    var token = objeto.get("Token").toString()


                    val editarShared=miShared.edit()

                        if( respuesta.get("resp").toString() == "true"){

                            //Toast.makeText(this@MainActivity,"EXITOSO",Toast.LENGTH_SHORT).show()

                            editarShared.putString("Token",token)

                            editarShared.apply()

                            intent = Intent(this@MainActivity,ViewMine::class.java)

                            startActivity(intent)

                        }
                        else
                        {
                            Toast.makeText(this@MainActivity,"USUARIO NO AUTORIZADO",Toast.LENGTH_SHORT).show()
                        }
                }

            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                println(t.stackTraceToString())
                Toast.makeText(this@MainActivity,"FALLIDO",Toast.LENGTH_SHORT).show()
            }


        })


    }

    private fun createService(): API {

        val gson = GsonBuilder()
            .setLenient()
            .create()

        val retrofit = Retrofit.Builder()
            .baseUrl("https://www.codigo-alfa.cl/Api/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        return retrofit.create(API::class.java)
    }





}