package com.example.pingermovile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pingermovile.Model.Machines
import com.example.pingermovile.ViewModel.MachineAdapter
import com.example.pingermovile.databinding.ActivityMainViewBinding

class MainView : AppCompatActivity() {

    private lateinit var binding: ActivityMainViewBinding

    val MachinesLHD: List<Machines> = listOf(

        Machines("LHD 440","Lectura:16:00:00","Sensor:15:58:00","Conexion:16:59:00",R.drawable.operativa),
        Machines("LHD 441","Lectura:16:00:00","Sensor:15:58:00","Conexion:16:59:00",R.drawable.operativa),
        Machines("LHD 442","Lectura:16:00:00","Sensor:15:58:00","Conexion:16:59:00",R.drawable.operativa),
        Machines("LHD 443","Lectura:16:00:00","Sensor:15:58:00","Conexion:16:59:00",R.drawable.fueraservicio),
        Machines("LHD 444","Lectura:16:00:00","Sensor:15:58:00","Conexion:16:59:00",R.drawable.fueraservicio),
        Machines("LHD 445","Lectura:16:00:00","Sensor:15:58:00","Conexion:16:59:00",R.drawable.fueraservicio),
        Machines("LHD 446","Lectura:16:00:00","Sensor:15:58:00","Conexion:16:59:00",R.drawable.operativa),
        Machines("LHD 447","Lectura:16:00:00","Sensor:15:58:00","Conexion:16:59:00",R.drawable.fueraservicio),


    )

    val MachinesTruck: List<Machines> = listOf(

        Machines("Truck 389","Lectura:16:00:00","Sensor:15:58:00","Conexion:16:59:00",R.drawable.camionoperativo),
        Machines("Truck 430","Lectura:16:00:00","Sensor:15:58:00","Conexion:16:59:00",R.drawable.camionoperativo),
        Machines("Truck 567","Lectura:16:00:00","Sensor:15:58:00","Conexion:16:59:00",R.drawable.camiondemora),
        Machines("Truck 568","Lectura:16:00:00","Sensor:15:58:00","Conexion:16:59:00",R.drawable.camiondemora),



        )

    val MachinesJumbo: List<Machines> = listOf(

        Machines("Jumbo 389","Lectura:16:00:00","Sensor:15:58:00","Conexion:16:59:00",R.drawable.jumbooperativo),
        Machines("Jumbo 430","Lectura:16:00:00","Sensor:15:58:00","Conexion:16:59:00",R.drawable.jumboreserva),
        Machines("Jumbo 567","Lectura:16:00:00","Sensor:15:58:00","Conexion:16:59:00",R.drawable.jumboreserva),
        Machines("Jumbo 568","Lectura:16:00:00","Sensor:15:58:00","Conexion:16:59:00",R.drawable.jumbooperativo),



        )



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainViewBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initRecycler()





    }



    fun initRecycler(){

        var maquina = intent.getStringExtra("Maquina").toString()


        var machineToadapter = MachinesLHD

        when (maquina) {
            "LHD" -> {
                machineToadapter = MachinesLHD
            }
            "TRUCK" -> {
                machineToadapter = MachinesTruck
            }
            "JUMBO" -> {
                machineToadapter = MachinesJumbo
            }
        }


        binding.recycleMachine.layoutManager = LinearLayoutManager(this)
        val adapter = MachineAdapter(machineToadapter)
        binding.recycleMachine.adapter = adapter

        adapter.setOnItemCLickListener(object : MachineAdapter.onItemClickListener{
            override fun onIntemClick(position: Int) {



                /*DECALRAR INTENT PARA CAMBIAR DE PAGINA*/
                var intent = Intent(this@MainView,MachineSingleView::class.java)

                /* DECLARAR VARIABLE A ENVIAR Y ASIGNAR VALOR*/
                var machineImageToView= machineToadapter.get(position).imagen


                /* DECLARAR VARIABLE A ENVIAR Y ASIGNAR VALOR*/
                var machineNameToView = machineToadapter.get(position).name

                /*AGREGAR A INTENT LA VARIABLE, PRIMERO ENTRE COMILLAS EL NOMBRE CON EL QUE SE GUARDARA, LUEGO LA VARIABLE QUE PASA DE VIEW*/
                intent.putExtra("img",machineImageToView)

                /*AGREGAR A INTENT LA VARIABLE, PRIMERO ENTRE COMILLAS EL NOMBRE CON EL QUE SE GUARDARA, LUEGO LA VARIABLE QUE PASA DE VIEW*/
                intent.putExtra("name",machineNameToView)
                startActivity(intent)


            }


        })



    }



}