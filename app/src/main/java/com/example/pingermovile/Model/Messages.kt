package com.example.pingermovile.Model

import java.sql.Time

data class Messages(

    val sender: String,
    val machineId: Int,
    val name: String,
    val body: String,
    val img: Int,

)
