package com.example.pingermovile.Model

data class Personaje(
    var char_id:Number,
    var name:String,
    var birthday:String,
    var occupation:List<String>,
    var img:String,
    var status:String,
    var nickname:String,
    var appearance: List<Number>,
    var portrayed: String,
    var category:String,
    var betterCallSaulAppearance: List<Number>

)