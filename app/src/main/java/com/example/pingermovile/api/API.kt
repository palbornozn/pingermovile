package com.example.pingermovile.api

import com.example.pingermovile.Model.Personaje
import com.example.pingermovile.Model.Usuario
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface API {
    /*
    @GET("characters")

    fun getPersonajes():Call<ArrayList<Personaje>>;
    */
    @FormUrlEncoded
    @POST("Acceso")
    fun login(
        @Field("Usuario")nombre: String,
        @Field("Password")password: String,
    ): Call<JsonObject>


}