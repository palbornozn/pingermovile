package com.example.pingermovile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.pingermovile.databinding.ActivityMachineSingleViewBinding
import com.example.pingermovile.databinding.MineSelectorBinding
import com.squareup.picasso.Picasso


private lateinit var  binding: ActivityMachineSingleViewBinding

class MachineSingleView : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        binding = ActivityMachineSingleViewBinding.inflate(layoutInflater)
        setContentView(binding.root)


        var img = intent.getIntExtra("img",1)


        var name = intent.getStringExtra("name")?.toString()



        if (img != null) {
            Picasso.get().load(img).into(binding.imgMachine)
        }


        binding.machineName.text = name

        binding.btnMessages.setOnClickListener{

            var intent = Intent(this,MessagesActivity::class.java)
            startActivity(intent)
        }
        binding.btnOperation.setOnClickListener{


            var intent = Intent(this,OperationView::class.java)
            startActivity(intent)

        }





    }
}