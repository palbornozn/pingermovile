package com.example.pingermovile

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.example.pingermovile.databinding.MineSelectorBinding

class ViewMine : AppCompatActivity() {

    private lateinit var binding: MineSelectorBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MineSelectorBinding.inflate(layoutInflater)
        setContentView(binding.root)


        /*VER INFOR SHARED */


        var miShared= getSharedPreferences("Memoria", Context.MODE_PRIVATE)

        val token =  miShared.getString("Token","No hay token")

        println(token)



        /*DECLARAR VARIABLE TIPO SPINNER*/
        val spinnerMaquina:Spinner = binding.snipnerMaquina
        val spinnerMine: Spinner = binding.spinerMina


        /*DECLARAR VARIABLE LISTA ELEMENTOS SPINNER*/
        val spinerItemMachine = listOf("JUMBO","LHD","TRUCK")
        val spinerItemMine = listOf("SUB6","ESMERALDA")



        val adaptadorMachine = ArrayAdapter(this,android.R.layout.simple_list_item_1,spinerItemMachine)
        val adaptadorMine = ArrayAdapter(this,android.R.layout.simple_list_item_1,spinerItemMine)


        spinnerMaquina.adapter = adaptadorMachine
        spinnerMine.adapter = adaptadorMine




        binding.imgUser.setOnClickListener {

            val intent = Intent(this, UserActivity::class.java)
            startActivity(intent)
            finish()


        }


        /*DECLARAR VARIABLE LISTA ELEMENTOS SPINNER*/
        spinnerMaquina.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                if(spinnerMaquina.selectedItem.toString()=="LHD"){
                    spinnerMine.setVisibility(View.VISIBLE)
                }else{
                    spinnerMine.setVisibility(View.INVISIBLE)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })




        binding.btnToRecycle.setOnClickListener{

            var maquina = binding.snipnerMaquina.selectedItem.toString();
            var mina = binding.spinerMina.selectedItem.toString()
            var intent = Intent(this, MainView::class.java)
            intent.putExtra("Maquina",maquina)
            intent.putExtra("mina",mina)
            startActivity(intent)


        }

    }




}


