package com.example.pingermovile

import android.content.ClipData
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pingermovile.Model.Messages
import com.example.pingermovile.ViewModel.MessagesAdapter
import com.example.pingermovile.databinding.ActivityMessagesBinding

class MessagesActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMessagesBinding

    var messages: List<Messages> = listOf(

            Messages("Remote",1,"LHD 441","Mensaje de prueba",R.drawable.operativa),
            Messages("Remote",2,"LHD 441","Hola, llega mensaje?",R.drawable.operativa),
            Messages("Remote",2,"LHD 441","Hola, si llega mensaje",R.drawable.operativa),


    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMessagesBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initRecycle()



        binding.btnMessages.setOnClickListener{

            if(binding.etMessages.text.isEmpty()){
                Toast.makeText(this@MessagesActivity,"El mensaje esta en blanco",Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(this@MessagesActivity,"Mensaje enviado",Toast.LENGTH_SHORT).show()
               var ItemMessage = Messages("App",1,"LHD 441",binding.etMessages.text.toString(),R.drawable.favicon)
                messages.toMutableList().add(ItemMessage)
                binding.etMessages.text.clear()

                var index = messages.size

                val adapter = MessagesAdapter(messages)
                binding.recycleViewMensajes.adapter = adapter

                adapter.notifyItemInserted(index)


            }
        }
    }


    fun initRecycle(){

        binding.recycleViewMensajes.layoutManager = LinearLayoutManager(this)
        val adapter = MessagesAdapter(messages)
        binding.recycleViewMensajes.adapter = adapter



    }
}