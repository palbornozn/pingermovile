package com.example.pingermovile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.pingermovile.databinding.ActivityUserBinding

class UserActivity : AppCompatActivity() {

    private lateinit var binding: ActivityUserBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUserBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.btnPassword.setOnClickListener{

            val password = binding.password
            val passwordCheck = binding.passwordCheck


            if( password.text.toString() == passwordCheck.text.toString()){

                    val intent = Intent(this,ViewMine::class.java)
                    startActivity(intent)


            }else{

                Toast.makeText(this,"Las contraseñas deben coincidir",Toast.LENGTH_LONG).show()

            }

        }
    }
}